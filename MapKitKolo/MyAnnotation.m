//
//  MyAnnotation.m
//  MapKitKolo
//
//  Created by Gabriel Zolnierczuk on 14/01/15.
//  Copyright (c) 2015 Gabriel Zolnierczuk. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation
-(id)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title
{
    if ((self = [super init]))
    {
        self.coordinate = coordinate;
        self.title = title;
    }
    return self;
}
@end

