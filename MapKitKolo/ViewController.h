//
//  ViewController.h
//  MapKitKolo
//
//  Created by Gabriel Zolnierczuk on 14/01/15.
//  Copyright (c) 2015 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

