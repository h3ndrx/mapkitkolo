//
//  ViewController.m
//  MapKitKolo
//
//  Created by Gabriel Zolnierczuk on 14/01/15.
//  Copyright (c) 2015 Gabriel Zolnierczuk. All rights reserved.
//

#import "ViewController.h"
#import "MyAnnotation.h"

@interface ViewController ()

@end

@implementation ViewController
CLLocationManager *locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mapView.delegate = self;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    
    _mapView.showsUserLocation = YES;
    
    
    NSURL *url = [NSURL URLWithString:@"http://bit.ly/MapKitApp"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    __block NSArray *points = [[NSArray alloc] init];

    
    
    
    
    //Synchronicznie
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    points = [dict objectForKey:@"points"];
    
    for (int i = 0; i < points.count; i++)
    {
        MyAnnotation *anno = [[MyAnnotation alloc]initWithCoordinate:CLLocationCoordinate2DMake([[points[i]objectForKey:@"lat"]doubleValue], [[points[i]objectForKey:@"lng"]doubleValue]) title:[points[i]objectForKey:@"title"]];
        
        anno.subtitle = [points[i]objectForKey:@"subtitle"];
        
        [_mapView addAnnotation:anno];
    }
    
    
    //Asynchronicznie
//    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"%@",str);
//        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        points = [dict objectForKey:@"points"];
//        
//        for (int i = 0; i < points.count; i++)
//        {
//            MyAnnotation *anno = [[MyAnnotation alloc]initWithCoordinate:CLLocationCoordinate2DMake([[points[i]objectForKey:@"lat"]doubleValue], [[points[i]objectForKey:@"lng"]doubleValue]) title:[points[i]objectForKey:@"title"]];
//            
//            anno.subtitle = [points[i]objectForKey:@"subtitle"];
//            
//            [_mapView addAnnotation:anno];
//        }
//    }];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{

    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    
    NSString *annotationId = @"PinViewAnnotation";
    MKPinAnnotationView *pinView = (MKPinAnnotationView*) [_mapView dequeueReusableAnnotationViewWithIdentifier:annotationId];
    
    if(!pinView)
    {
        pinView = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:annotationId];
        
        //pinView.pinColor = MKPinAnnotationColorPurple;
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
    }
    else
    {
        pinView.annotation = annotation;
    }
    
    pinView.image = [UIImage imageNamed:@"asd"];
    return pinView;
}

@end
