//
//  AppDelegate.h
//  MapKitKolo
//
//  Created by Gabriel Zolnierczuk on 14/01/15.
//  Copyright (c) 2015 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

