//
//  MyAnnotation.h
//  MapKitKolo
//
//  Created by Gabriel Zolnierczuk on 14/01/15.
//  Copyright (c) 2015 Gabriel Zolnierczuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation>
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

-(id)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title;
@end
